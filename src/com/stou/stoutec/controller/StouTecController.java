package com.stou.stoutec.controller;

import com.stou.stoutec.model.StouTecModel;
import com.stou.stoutec.service.StouTecService;
import com.stou.stoutec.view.ApplicantDescView;
import com.stou.stoutec.view.StouTecView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StouTecController implements ActionListener {

  private StouTecModel model;
  private StouTecView view;
  private StouTecService service;

  public StouTecController(StouTecModel model, StouTecView view, StouTecService service) {
    this.model = model;
    this.view = view;
    this.service = service;

    view.getBtnOk().addActionListener(this);
    view.getRbMale().addActionListener(this);
    view.getRbFemale().addActionListener(this);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String command = e.getActionCommand();

    if (command.equalsIgnoreCase("OK")) {
      service.setApIdText(view.getTxtApId().getText());
      service.setApNameText(view.getTxtApName().getText());
      service.setApAddressText(view.getTxtApAddress().getText());
      service.setApChwText(view.getTxtApChw().getText());
      service.setApSexText(view.getBtgSex().getSelection().getActionCommand());
      service.setApEdText(view.getTxtApEd().getText());
      service.setApMajText(view.getTxtApMaj().getText());

      service.setQualDesc1Text((String) view.getCbQualDesc1().getSelectedItem());
      service.setQualDesc2Text((String) view.getCbQualDesc2().getSelectedItem());
      service.setQualDesc3Text((String) view.getCbQualDesc3().getSelectedItem());
      service.setQualDesc4Text((String) view.getCbQualDesc4().getSelectedItem());
      service.setQualDesc5Text((String) view.getCbQualDesc5().getSelectedItem());

      service.setPosName1Text((String) view.getCbPosition1().getSelectedItem());
      service.setPosName2Text((String) view.getCbPosition2().getSelectedItem());
      service.setPosName3Text((String) view.getCbPosition3().getSelectedItem());

      if (service.validateAllInputAreRequired(view) == false) {
        view.showErrorDialog();
      } else {
        ApplicantDescView appDescView = new ApplicantDescView("Applicant Description", model);
        appDescView.setVisible(true);
      }
    }
  }
}
