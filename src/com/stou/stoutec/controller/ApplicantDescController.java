package com.stou.stoutec.controller;

import com.stou.stoutec.model.StouTecModel;
import com.stou.stoutec.service.ApplicantDescService;
import com.stou.stoutec.view.ApplicantDescView;

public class ApplicantDescController {
  private StouTecModel model;
  private ApplicantDescView view;
  private ApplicantDescService service;

  public ApplicantDescController(StouTecModel model, ApplicantDescView view, ApplicantDescService service) {
    this.model = model;
    this.view = view;
    this.service = service;
  }

}
