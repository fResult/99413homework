package com.stou.stoutec;

import com.stou.stoutec.controller.ApplicantDescController;
import com.stou.stoutec.controller.StouTecController;
import com.stou.stoutec.model.StouTecModel;
import com.stou.stoutec.service.ApplicantDescService;
import com.stou.stoutec.service.StouTecService;
import com.stou.stoutec.view.ApplicantDescView;
import com.stou.stoutec.view.StouTecView;

public class StouTecApplication {

  public static void main(String[] args) {

    StouTecView view = new StouTecView();
    StouTecModel model = new StouTecModel();
    StouTecService service = new StouTecService(model);
    view.setVisible(true);

    ApplicantDescView appDescView = new ApplicantDescView("Applicant Description", model);
//    ApplicantDescService appDescService = new ApplicantDescService();
    new StouTecController(model, view, service);
//    new ApplicantDescController(model, appDescView, appDescService);

  }
}
