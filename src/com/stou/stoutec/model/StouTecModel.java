package com.stou.stoutec.model;

public class StouTecModel {

  private String apId, apName, apAddress, apChw, apSex, apEd, apMaj;
  private String qualDesc1, qualDesc2, qualDesc3, qualDesc4, qualDesc5;
  private String posName1;
  private String posName2;

  public String getQualDesc4() {
    return qualDesc4;
  }

  public void setQualDesc4(String qualDesc4) {
    this.qualDesc4 = qualDesc4;
  }

  public String getQualDesc5() {
    return qualDesc5;
  }

  public void setQualDesc5(String qualDesc5) {
    this.qualDesc5 = qualDesc5;
  }

  private String posName3;

  public String getApId() {
    return apId;
  }

  public void setApId(String apId) {
    this.apId = apId;
  }

  public String getApName() {
    return apName;
  }

  public void setApName(String apName) {
    this.apName = apName;
  }

  public String getApAddress() {
    return apAddress;
  }

  public void setApAddress(String apAddress) {
    this.apAddress = apAddress;
  }

  public String getApChw() {
    return apChw;
  }

  public void setApChw(String apChw) {
    this.apChw = apChw;
  }

  public String getApSex() {
    return apSex;
  }

  public void setApSex(String apSex) {
    this.apSex = apSex;
  }

  public String getApEd() {
    return apEd;
  }

  public void setApEd(String apEd) {
    this.apEd = apEd;
  }

  public String getApMaj() {
    return apMaj;
  }

  public void setApMaj(String apMaj) {
    this.apMaj = apMaj;
  }

  public String getQualDesc1() {
    return qualDesc1;
  }

  public void setQualDesc1(String qualDesc1) {
    this.qualDesc1 = qualDesc1;
  }

  public String getQualDesc2() {
    return qualDesc2;
  }

  public void setQualDesc2(String qualDesc2) {
    this.qualDesc2 = qualDesc2;
  }

  public String getQualDesc3() {
    return qualDesc3;
  }

  public void setQualDesc3(String qualDesc3) {
    this.qualDesc3 = qualDesc3;
  }

  public String getPosName1() {
    return posName1;
  }

  public void setPosName1(String posName1) {
    this.posName1 = posName1;
  }

  public String getPosName2() {
    return posName2;
  }

  public void setPosName2(String posName2) {
    this.posName2 = posName2;
  }

  public String getPosName3() {
    return posName3;
  }

  public void setPosName3(String posName3) {
    this.posName3 = posName3;
  }

  @Override
  public String toString() {
    return "StouTecModel{" +
      "apId='" + apId + '\'' +
      ", apName='" + apName + '\'' +
      ", apAddress='" + apAddress + '\'' +
      ", apChw='" + apChw + '\'' +
      ", apSex='" + apSex + '\'' +
      ", apEd='" + apEd + '\'' +
      ", apMaj='" + apMaj + '\'' +
      ", qualDesc1='" + qualDesc1 + '\'' +
      ", qualDesc2='" + qualDesc2 + '\'' +
      ", qualDesc3='" + qualDesc3 + '\'' +
      ", qualDesc4='" + qualDesc4 + '\'' +
      ", qualDesc5='" + qualDesc5 + '\'' +
      ", posName1='" + posName1 + '\'' +
      ", posName2='" + posName2 + '\'' +
      ", posName3='" + posName3 + '\'' +
      '}';
  }
}
