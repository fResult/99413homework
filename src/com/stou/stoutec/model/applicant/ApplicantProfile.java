package com.stou.stoutec.model.applicant;

import com.stou.stoutec.model.position.Position;
import com.stou.stoutec.model.qualification.Qualification;

public class ApplicantProfile {
  private String apId;
  private String apName;
  private String apAddress;
  private String apChw;
  private String apSex;
  private String apEd;
  private String apMaj;
  private Qualification qualification;
  private Position position;

  public String getApId() {
    return apId;
  }

  public void setApId(String apId) {
    this.apId = apId;
  }

  public String getApName() {
    return apName;
  }

  public void setApName(String apName) {
    this.apName = apName;
  }

  public String getApAddress() {
    return apAddress;
  }

  public void setApAddress(String apAddress) {
    this.apAddress = apAddress;
  }

  public String getApChw() {
    return apChw;
  }

  public void setApChw(String apChw) {
    this.apChw = apChw;
  }

  public String getApSex() {
    return apSex;
  }

  public void setApSex(String apSex) {
    this.apSex = apSex;
  }

  public String getApEd() {
    return apEd;
  }

  public void setApEd(String apEd) {
    this.apEd = apEd;
  }

  public String getApMaj() {
    return apMaj;
  }

  public void setApMaj(String apMaj) {
    this.apMaj = apMaj;
  }

  public Qualification getQualification() {
    return qualification;
  }

  public void setQualification(Qualification qualification) {
    this.qualification = qualification;
  }

  public Position getPosition() {
    return position;
  }

  public void setPosition(Position position) {
    this.position = position;
  }

  @Override
  public String toString() {
    return "ApplicantProfile{" +
      "apId='" + apId + '\'' +
      ", apName='" + apName + '\'' +
      ", apAddress='" + apAddress + '\'' +
      ", apChw='" + apChw + '\'' +
      ", apSex='" + apSex + '\'' +
      ", apEd='" + apEd + '\'' +
      ", apMaj='" + apMaj + '\'' +
      ", qualification=" + qualification +
      ", position=" + position +
      '}';
  }
}
