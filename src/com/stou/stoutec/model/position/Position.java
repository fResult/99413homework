package com.stou.stoutec.model.position;

public class Position {
  private String posId;
  private String posName;

  public String getPosId() {
    return posId;
  }

  public void setPosId(String posId) {
    this.posId = posId;
  }

  public String getPosName() {
    return posName;
  }

  public void setPosName(String posName) {
    this.posName = posName;
  }

  @Override
  public String toString() {
    return "Position{" +
      "posId='" + posId + '\'' +
      ", posName='" + posName + '\'' +
      '}';
  }

}
