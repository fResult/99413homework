package com.stou.stoutec.constant;

import java.awt.*;

public final class ConstVal {

  public static final int FONT_SIZE = 16;
  public static final String FONT_STYLE = "Tahoma";

  public static final Font BOLD_TAHOMA_18 = new Font(FONT_STYLE, Font.BOLD, 18);
  public static final Font PLAIN_TAHOMA_16 = new Font(FONT_STYLE, Font.PLAIN, 16);
  public static final Font BOLD_TAHOMA_16 = new Font(FONT_STYLE, Font.BOLD, 16);

  public static final String[] QUAL_DESCRIPTIONS = {"Web_ASP", "C++", "Database Administrator, DB2", "Database Administrator, ORACLE",
    "Graphic Design", "Java", "Management", "Network", "Secretarial work, 45 words/min", "Secretarial work, 60 words/min",
    "System Analyst, Level 1", "System Analyst, Level 2", "Visual Basic"};
  public static final String[] POS_NAMES = {"Technical Programmer", "Web Developer", "General Manager", "ICT Specialist",
    "E-Business Analyst", "ICT Documentor", "Database Administrator", "PC Administrator", "Network Specialist", "ICT Manager"};

  public static final int LBL_WIDTH = 400;
  public static final int USER_INPUT_WIDTH = 300;
  public static final int COMPONENT_HEIGHT = 25;

  public static final int BUTTON_WIDTH = 100;
  public static final int BUTTON_HEIGHT = 30;

  public static final int LBL_START_X = 35;
  public static final int USER_INPUT_START_X = 350;

  public static final int LBL_WIDTH_APPDESC = 600;
}
