package com.stou.stoutec.service;

import com.stou.stoutec.model.StouTecModel;
import com.stou.stoutec.view.StouTecView;

public class StouTecService {

  private StouTecModel model;

  public StouTecService(StouTecModel model) {
    this.model = model;
  }

  public void setApIdText(String apId) {
    model.setApId(apId);
    if (model.getApId().equals("")) {
      model.setApId("กรุณากรอกเลขประจำตัวผู้สมัคร (AP_ID)");
    }
    System.out.println("ApId: " + model.getApId());
  }

  public void setApNameText(String apName) {
    model.setApName(apName);
    if (model.getApName().equals("")) {
      model.setApName("กรุณากรอกชื่อ-นามสกุล AP_NAME");
    }
    System.out.println("ApName: " + model.getApName());
  }

  public void setApAddressText(String apAddress) {
    model.setApAddress(apAddress);
    if (model.getApAddress().equals("")) {
      model.setApAddress("กรุณากรอกที่อยู่ (AP_ADDRESS)");
    }
    System.out.println("ApAddress: " + model.getApAddress());
  }

  public void setApChwText(String apChw) {
    model.setApChw(apChw);
    if (model.getApChw().equals("")) {
      model.setApChw("กรุณากรอกจังหวัด (AP_CHW)");
    }
    System.out.println("ApChw: " + model.getApChw());
  }

  public void setApSexText(String apSex) {
    model.setApSex(apSex);
    System.out.println("ApSex: " + model.getApSex());
  }

  public void setApEdText(String apEd) {
    model.setApEd(apEd);
    if (model.getApEd().equals("")) {
      model.setApEd("กรุณากรอกวุฒิการศึกษาสูงสุด (AP_ED)");
    }
    System.out.println("ApEd: " + model.getApEd());
  }

  public void setApMajText(String apMaj) {
    model.setApMaj(apMaj);
    if (model.getApMaj().equals("")) {
      model.setApMaj("กรุณากรอกวิชาเอก (AP_MAJ)");
    }
    System.out.println("ApMaj: " + model.getApMaj());
  }

  public void setQualDesc1Text(String qualDesc1) {
    model.setQualDesc1(qualDesc1);
    if (model.getQualDesc1() == null) {
      model.setQualDesc1("กรุณาเลือกความรู้ความสามารถ 1");
    }
    System.out.println("qualDesc1: " + model.getQualDesc1());
  }

  public void setQualDesc2Text(String qualDesc2) {
    model.setQualDesc2(qualDesc2);
    if (model.getQualDesc2() == null) {
      model.setQualDesc2("กรุณาเลือกความรู้ความสามารถ 2");
    }
    System.out.println("QualDesc2: " + model.getQualDesc2());
  }

  public void setQualDesc3Text(String qualDesc3) {
    model.setQualDesc3(qualDesc3);
    if (model.getQualDesc3() == null) {
      model.setQualDesc3("กรุณาเลือกความรู้ความสามารถ 3");
    }
    System.out.println("QualDesc3: " + model.getQualDesc3());
  }

  public void setQualDesc4Text(String qualDesc4) {
    model.setQualDesc4(qualDesc4);
    if (model.getQualDesc4() == null) {
      model.setQualDesc4("กรุณาเลือกความรู้ความสามารถ 4");
    }
    System.out.println("QualDesc4: " + model.getQualDesc4());
  }

  public void setQualDesc5Text(String qualDesc5) {
    model.setQualDesc5(qualDesc5);
    if (model.getQualDesc5() == null) {
      model.setQualDesc5("กรุณาเลือกความรู้ความสามารถ 5");
    }
    System.out.println("QualDesc5: " + model.getQualDesc5());
  }

  public void setPosName1Text(String posName1) {
    model.setPosName1(posName1);
    if (model.getPosName1() == null) {
      model.setPosName1("กรุณาเลือกตำแหน่งที่ต้องการสมัคร 1");
    }
    System.out.println("PosName1 " + model.getPosName1());
  }

  public void setPosName2Text(String posName2) {
    model.setPosName2(posName2);
    if (model.getPosName2() == null) {
      model.setPosName2("กรุณาเลือกตำแหน่งที่ต้องการสมัคร 2");
    }
    System.out.println("PosName2 " + model.getPosName2());
  }

  public void setPosName3Text(String posName3) {
    System.out.println(posName3);
    model.setPosName3(posName3);
    if (model.getPosName3() == null) {
      model.setPosName3("กรุณาเลือกตำแหน่งที่ต้องการสมัคร 3");
    }
    System.out.println("PosName3 " + model.getPosName3());
  }

  public boolean validateAllInputAreRequired(StouTecView view) {
    if ("".equals(view.getTxtApId().getText()) || "".equals(view.getTxtApName().getText()) ||
      "".equals(view.getTxtApAddress().getText()) || "".equals(view.getTxtApChw().getText()) ||
      "".equals(view.getTxtApEd().getText()) || "".equals(view.getTxtApMaj().getText()) ||
      view.getCbQualDesc1().getSelectedItem() == null || view.getCbQualDesc2().getSelectedItem() == null ||
      view.getCbQualDesc3().getSelectedItem() == null || view.getCbQualDesc4().getSelectedItem() == null ||
      view.getCbQualDesc5().getSelectedItem() == null || view.getCbPosition1().getSelectedItem() == null ||
      view.getCbPosition2().getSelectedItem() == null || view.getCbPosition3().getSelectedItem() == null) {

      return false;
    } else {

      return true;
    }
  }
}
