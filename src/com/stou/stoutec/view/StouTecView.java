package com.stou.stoutec.view;


import com.stou.stoutec.service.util.MyComboBoxRenderer;

import javax.swing.*;
import java.awt.*;

import static com.stou.stoutec.constant.ConstVal.*;

public class StouTecView extends JFrame {

  private Container container;

  private JLabel lblCompanyName, lblTitle, lblDescription, lblApId, lblApName, lblApAddress, lblApChw, lblApSex,
    lblApEd, lblApMaj, lblQualDescTitle, lblQualDesc1, lblQualDesc2, lblQualDesc3, lblQualDesc4, lblQualDesc5,
    lblPosNameTitle, lblPosName1, lblPosName2, lblPosName3;
  private JTextField txtApId, txtApName, txtApAddress, txtApChw, txtApEd, txtApMaj;
  private JButton btnOk;
  private JComboBox<String> cbQualDesc1, cbQualDesc2, cbQualDesc3, cbQualDesc4, cbQualDesc5,
    cbPosition1, cbPosition2, cbPosition3;
  private JRadioButton rbMale, rbFemale;
  private ButtonGroup btgSex;

  private JOptionPane dialogError;

  /** TODO Move Applicant Desc visible true into this class */
  private ApplicantDescView appDescView;

  public StouTecView() {
    super("STOU-TEC Form");
    initComponents();

    btnOk.setActionCommand("OK");
    rbMale.setActionCommand("ชาย");
    rbFemale.setActionCommand("หญิง");
  }

  public void initComponents() {
    container = getContentPane();
    container.setLayout(null);

    this.defineLabel();
    this.defineTextField();
    this.defineRadioButton();
    this.defineButtonGroup();
    this.defineComboBox();
    this.defineButton();

    this.setLabelsBounds();
    this.setUserInputsAndButtonBounds();
    this.setLabelsFont();
    this.setUserInputsAndButtonFont();

    this.setDefaultValueForRdBtnsAndComboBoxs();

    this.addComponentsIntoContainer();

    this.setSize(700, 800);
    System.out.println("lblCompanyNamePositionXStart: " + (700 / 2 - lblCompanyName.getPreferredSize().width / 2));
    System.out.println("lblTitlePositionXStart: " + (700 / 2 - lblTitle.getPreferredSize().width / 2));
    System.out.println("lblDescriptionPositionXStart: " + (700 / 2 - lblDescription.getPreferredSize().width / 2));
    this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    this.setLocationRelativeTo(null);
    this.setResizable(false);
  }

  public void defineLabel() {
    lblCompanyName = new JLabel("บริษัท STOU-TEC จำกัด");
    lblTitle = new JLabel("<html><div style=\"text-align: center\">แบบฟอร์มใบสมัครพนักงาน</div></html>");
    lblDescription = new JLabel("<html><div style=\"text-align: center\">ใบสมัครสำหรับงานด้าน ICT นี้ ให้ผู้สมัครกรอกรายละเอียดต่อไปนี้เพื่อที่<br/>"
      + "STOU-TEC จำกัด ทำการคัดเลือกตามความเหมาะสม</div></html>");

    lblApId = new JLabel("เลขประจำตัวของผู้สมัคร (AP_ID) : ");
    lblApName = new JLabel("ชื่อ-นามสกุล (AP_NAME) : ");
    lblApAddress = new JLabel("ที่อยู่ (AP_ADDRESS) : ");
    lblApChw = new JLabel("จังหวัด (AP_CHW) : ");
    lblApSex = new JLabel("เพศ (AP_SEX) : ");
    lblApEd = new JLabel("<html>วุฒิการศึกษาขั้นสูงสุด (AP_ED) : <br/> (เช่น B.S., B.A., M.S., M.BA., M.A.)</html>");
    lblApMaj = new JLabel("วิชาเอก (AP_MAJ) : ");

    lblQualDescTitle = new JLabel("ความรู้ความสามารถหรือคุณสมบัติเฉพาะตำแหน่ง (QUAL_DESC) : (ตอบได้ไม่เกิน 5 รายการ)");
    lblQualDesc1 = new JLabel("ความรู้ความสามารถ 1");
    lblQualDesc2 = new JLabel("ความรู้ความสามารถ 2");
    lblQualDesc3 = new JLabel("ความรู้ความสามารถ 3");
    lblQualDesc4 = new JLabel("ความรู้ความสามารถ 4");
    lblQualDesc5 = new JLabel("ความรู้ความสามารถ 5");

    lblPosNameTitle = new JLabel("ตำแหน่งที่ต้องการสมัคร (POS_NAME) : (ตอบได้ไม่เกิน 3 ตำแหน่ง)");
    lblPosName1 = new JLabel("ตำแหน่งที่ต้องการสมัคร 1");
    lblPosName2 = new JLabel("ตำแหน่งที่ต้องการสมัคร 2");
    lblPosName3 = new JLabel("ตำแหน่งที่ต้องการสมัคร 3");
  }

  public void addComponentsIntoContainer() {
    container.add(lblCompanyName);
    container.add(lblTitle);
    container.add(lblDescription);
    container.add(lblApId);
    container.add(lblApName);
    container.add(lblApAddress);
    container.add(lblApChw);
    container.add(lblApSex);
    container.add(lblApEd);
    container.add(lblApMaj);

    container.add(lblQualDescTitle);
    container.add(lblQualDesc1);
    container.add(lblQualDesc2);
    container.add(lblQualDesc3);
    container.add(lblQualDesc4);
    container.add(lblQualDesc5);

    container.add(lblPosNameTitle);
    container.add(lblPosName1);
    container.add(lblPosName2);
    container.add(lblPosName3);

    container.add(txtApId);
    container.add(txtApName);
    container.add(txtApAddress);
    container.add(txtApChw);
    container.add(txtApEd);
    container.add(txtApMaj);

    container.add(rbMale);
    container.add(rbFemale);

    container.add(cbQualDesc1);
    container.add(cbQualDesc2);
    container.add(cbQualDesc3);
    container.add(cbQualDesc4);
    container.add(cbQualDesc5);

    container.add(cbPosition1);
    container.add(cbPosition2);
    container.add(cbPosition3);

    container.add(btnOk);
  }

  public void setLabelsFont() {
    lblCompanyName.setFont(BOLD_TAHOMA_18);
    lblTitle.setFont(BOLD_TAHOMA_16);
    lblDescription.setFont(BOLD_TAHOMA_16);

    lblApId.setFont(PLAIN_TAHOMA_16);
    lblApName.setFont(PLAIN_TAHOMA_16);
    lblApAddress.setFont(PLAIN_TAHOMA_16);
    lblApChw.setFont(PLAIN_TAHOMA_16);
    lblApSex.setFont(PLAIN_TAHOMA_16);
    lblApEd.setFont(PLAIN_TAHOMA_16);
    lblApMaj.setFont(PLAIN_TAHOMA_16);

    lblQualDescTitle.setFont(BOLD_TAHOMA_16);
    lblQualDesc1.setFont(PLAIN_TAHOMA_16);
    lblQualDesc2.setFont(PLAIN_TAHOMA_16);
    lblQualDesc3.setFont(PLAIN_TAHOMA_16);
    lblQualDesc4.setFont(PLAIN_TAHOMA_16);
    lblQualDesc5.setFont(PLAIN_TAHOMA_16);

    lblPosNameTitle.setFont(BOLD_TAHOMA_16);
    lblPosName1.setFont(PLAIN_TAHOMA_16);
    lblPosName2.setFont(PLAIN_TAHOMA_16);
    lblPosName3.setFont(PLAIN_TAHOMA_16);
  }

  public void setUserInputsAndButtonFont() {
    txtApId.setFont(PLAIN_TAHOMA_16);
    txtApName.setFont(PLAIN_TAHOMA_16);
    txtApAddress.setFont(PLAIN_TAHOMA_16);
    txtApChw.setFont(PLAIN_TAHOMA_16);

    rbMale.setFont(PLAIN_TAHOMA_16);
    rbFemale.setFont(PLAIN_TAHOMA_16);

    cbQualDesc1.setFont(PLAIN_TAHOMA_16);
    cbQualDesc2.setFont(PLAIN_TAHOMA_16);
    cbQualDesc3.setFont(PLAIN_TAHOMA_16);
    cbQualDesc4.setFont(PLAIN_TAHOMA_16);
    cbQualDesc5.setFont(PLAIN_TAHOMA_16);

    cbPosition1.setFont(PLAIN_TAHOMA_16);
    cbPosition2.setFont(PLAIN_TAHOMA_16);
    cbPosition3.setFont(PLAIN_TAHOMA_16);

    btnOk.setFont(BOLD_TAHOMA_16);
  }

  public void defineTextField() {
    txtApId = new JTextField();
    txtApName = new JTextField();
    txtApAddress = new JTextField();
    txtApChw = new JTextField();
    txtApEd = new JTextField();
    txtApMaj = new JTextField();
  }

  public void defineRadioButton() {
    rbMale = new JRadioButton("ชาย");
    rbFemale = new JRadioButton("หญิง");
  }

  public void defineButtonGroup() {
    btgSex = new ButtonGroup();
    btgSex.add(rbMale);
    btgSex.add(rbFemale);
  }

  public void defineComboBox() {
    cbQualDesc1 = new JComboBox<>(QUAL_DESCRIPTIONS);
    cbQualDesc2 = new JComboBox<>(QUAL_DESCRIPTIONS);
    cbQualDesc3 = new JComboBox<>(QUAL_DESCRIPTIONS);
    cbQualDesc4 = new JComboBox<>(QUAL_DESCRIPTIONS);
    cbQualDesc5 = new JComboBox<>(QUAL_DESCRIPTIONS);

    cbPosition1 = new JComboBox<>(POS_NAMES);
    cbPosition2 = new JComboBox<>(POS_NAMES);
    cbPosition3 = new JComboBox<>(POS_NAMES);
  }

  public void defineButton() {
    btnOk = new JButton("ตกลง");
    btnOk.setBorder(null);
  }

  public void showErrorDialog() {
    JOptionPane.showMessageDialog(null, "กรุณาใส่ข้อมูลให้ครบถ้วน", "Error", JOptionPane.ERROR_MESSAGE);
  }

  public void setDefaultValueForRdBtnsAndComboBoxs() {
    rbMale.setSelected(true);

    cbQualDesc1.setRenderer(new MyComboBoxRenderer("เลือกความรู้ความสามารถ 1"));
    cbQualDesc2.setRenderer(new MyComboBoxRenderer("เลือกความรู้ความสามารถ 2"));
    cbQualDesc3.setRenderer(new MyComboBoxRenderer("เลือกความรู้ความสามารถ 3"));
    cbQualDesc4.setRenderer(new MyComboBoxRenderer("เลือกความรู้ความสามารถ 4"));
    cbQualDesc5.setRenderer(new MyComboBoxRenderer("เลือกความรู้ความสามารถ 5"));

    cbQualDesc1.setSelectedIndex(-1);
    cbQualDesc2.setSelectedIndex(-1);
    cbQualDesc3.setSelectedIndex(-1);
    cbQualDesc4.setSelectedIndex(-1);
    cbQualDesc5.setSelectedIndex(-1);

    cbPosition1.setRenderer(new MyComboBoxRenderer("เลือกตำแหน่งที่ต้องการสมัคร 1"));
    cbPosition2.setRenderer(new MyComboBoxRenderer("เลือกตำแหน่งที่ต้องการสมัคร 2"));
    cbPosition3.setRenderer(new MyComboBoxRenderer("เลือกตำแหน่งที่ต้องการสมัคร 3"));

    cbPosition1.setSelectedIndex(-1);
    cbPosition2.setSelectedIndex(-1);
    cbPosition3.setSelectedIndex(-1);
  }

  public void setLabelsBounds() {
    lblCompanyName.setBounds(254, 10, 700, COMPONENT_HEIGHT);
    lblTitle.setBounds(252, 50, 700, COMPONENT_HEIGHT);
    lblDescription.setBounds(96, 80, 700, 40);
    lblApId.setBounds(LBL_START_X, 135, LBL_WIDTH, COMPONENT_HEIGHT);
    lblApName.setBounds(LBL_START_X, 165, LBL_WIDTH, COMPONENT_HEIGHT);
    lblApAddress.setBounds(LBL_START_X, 195, LBL_WIDTH, COMPONENT_HEIGHT);
    lblApChw.setBounds(LBL_START_X, 225, LBL_WIDTH, COMPONENT_HEIGHT);
    lblApSex.setBounds(LBL_START_X, 255, LBL_WIDTH, COMPONENT_HEIGHT);
    lblApEd.setBounds(LBL_START_X, 285, LBL_WIDTH, 40);
    lblApMaj.setBounds(LBL_START_X, 335, LBL_WIDTH, COMPONENT_HEIGHT);

    lblQualDescTitle.setBounds(13, 370, 700, COMPONENT_HEIGHT);
    lblQualDesc1.setBounds(LBL_START_X, 400, LBL_WIDTH, COMPONENT_HEIGHT);
    lblQualDesc2.setBounds(LBL_START_X, 430, LBL_WIDTH, COMPONENT_HEIGHT);
    lblQualDesc3.setBounds(LBL_START_X, 460, LBL_WIDTH, COMPONENT_HEIGHT);
    lblQualDesc4.setBounds(LBL_START_X, 490, LBL_WIDTH, COMPONENT_HEIGHT);
    lblQualDesc5.setBounds(LBL_START_X, 520, LBL_WIDTH, COMPONENT_HEIGHT);

    lblPosNameTitle.setBounds(13, 555, 700, COMPONENT_HEIGHT);
    lblPosName1.setBounds(LBL_START_X, 585, LBL_WIDTH, COMPONENT_HEIGHT);
    lblPosName2.setBounds(LBL_START_X, 615, LBL_WIDTH, COMPONENT_HEIGHT);
    lblPosName3.setBounds(LBL_START_X, 645, LBL_WIDTH, COMPONENT_HEIGHT);
  }

  public void setUserInputsAndButtonBounds() {
    txtApId.setBounds(USER_INPUT_START_X, 135, USER_INPUT_WIDTH, COMPONENT_HEIGHT);
    txtApName.setBounds(USER_INPUT_START_X, 165, USER_INPUT_WIDTH, COMPONENT_HEIGHT);
    txtApAddress.setBounds(USER_INPUT_START_X, 195, USER_INPUT_WIDTH, COMPONENT_HEIGHT);
    txtApChw.setBounds(USER_INPUT_START_X, 225, USER_INPUT_WIDTH, COMPONENT_HEIGHT);
    txtApEd.setBounds(USER_INPUT_START_X, 285, USER_INPUT_WIDTH, COMPONENT_HEIGHT);
    txtApMaj.setBounds(USER_INPUT_START_X, 325, USER_INPUT_WIDTH, COMPONENT_HEIGHT);

    rbMale.setBounds(USER_INPUT_START_X, 255, 60, COMPONENT_HEIGHT);
    rbFemale.setBounds(415, 255, USER_INPUT_WIDTH, COMPONENT_HEIGHT);

    cbQualDesc1.setBounds(USER_INPUT_START_X, 400, USER_INPUT_WIDTH, COMPONENT_HEIGHT);
    cbQualDesc2.setBounds(USER_INPUT_START_X, 430, USER_INPUT_WIDTH, COMPONENT_HEIGHT);
    cbQualDesc3.setBounds(USER_INPUT_START_X, 460, USER_INPUT_WIDTH, COMPONENT_HEIGHT);
    cbQualDesc4.setBounds(USER_INPUT_START_X, 490, USER_INPUT_WIDTH, COMPONENT_HEIGHT);
    cbQualDesc5.setBounds(USER_INPUT_START_X, 520, USER_INPUT_WIDTH, COMPONENT_HEIGHT);

    cbPosition1.setBounds(USER_INPUT_START_X, 585, USER_INPUT_WIDTH, COMPONENT_HEIGHT);
    cbPosition2.setBounds(USER_INPUT_START_X, 615, USER_INPUT_WIDTH, COMPONENT_HEIGHT);
    cbPosition3.setBounds(USER_INPUT_START_X, 645, USER_INPUT_WIDTH, COMPONENT_HEIGHT);

    btnOk.setBounds(300, 700, BUTTON_WIDTH, BUTTON_HEIGHT);
  }

  public JTextField getTxtApId() {
    return txtApId;
  }

  public JTextField getTxtApName() {
    return txtApName;
  }

  public JTextField getTxtApAddress() {
    return txtApAddress;
  }

  public JTextField getTxtApChw() {
    return txtApChw;
  }

  public JTextField getTxtApEd() {
    return txtApEd;
  }

  public JTextField getTxtApMaj() {
    return txtApMaj;
  }

  public JButton getBtnOk() {
    return btnOk;
  }

  public JComboBox<String> getCbQualDesc1() {
    return cbQualDesc1;
  }

  public JComboBox<String> getCbQualDesc2() {
    return cbQualDesc2;
  }

  public JComboBox<String> getCbQualDesc3() {
    return cbQualDesc3;
  }

  public JComboBox<String> getCbQualDesc4() {
    return cbQualDesc4;
  }

  public JComboBox<String> getCbQualDesc5() {
    return cbQualDesc5;
  }

  public JComboBox<String> getCbPosition1() {
    return cbPosition1;
  }

  public JComboBox<String> getCbPosition2() {
    return cbPosition2;
  }

  public JComboBox<String> getCbPosition3() {
    return cbPosition3;
  }

  public JRadioButton getRbMale() {
    return rbMale;
  }

  public JRadioButton getRbFemale() {
    return rbFemale;
  }

  public ButtonGroup getBtgSex() {
    return btgSex;
  }

  public JOptionPane getDialogError() {
    return dialogError;
  }
}
