package com.stou.stoutec.view;

import com.stou.stoutec.model.StouTecModel;

import javax.swing.*;
import java.awt.*;

import static com.stou.stoutec.constant.ConstVal.*;

/**
 * TODO Create ApplicantDescService and ApplicantDescController
 */
public class ApplicantDescView extends JFrame {

  private StouTecModel model;

  private Container container = this.getContentPane();

  private JLabel lblTitle, lblApId, lblApName, lblApAddress, lblApChw, lblApSex, lblApEd, lblApMaj,
    lblQualDesc1, lblQualDesc2, lblQualDesc3, lblQualDesc4, lblQualDesc5, lblPosName1, lblPosName2, lblPosName3;

  private JButton btnEdit;

  public ApplicantDescView(String title, StouTecModel model) {
    super(title);

    this.model = model;

    this.setSize(700, 800);
    this.setLayout(null);
    this.setResizable(false);
    this.setLocationRelativeTo(null);

    initComponents();
    System.out.println("Start lblTitle_x_center: " + (700 / 2 - lblTitle.getPreferredSize().width / 2));
    System.out.println("Start btnEdit_x_center: " + (700 / 2 - btnEdit.getPreferredSize().width / 2));
  }

  public void initComponents() {
    this.defineLabels();
    this.defineButton();

    this.setLabelsFont();
    this.setLabelsBounds();
    this.setButtonBounds();

    this.addComponentsIntoContainer();
  }

  public void defineLabels() {
    lblTitle = new JLabel("รายละเอียดผู้สมัคร");
    lblApId = new JLabel("เลขประจำตัวผู้สมัคร: " + model.getApId());
    lblApName = new JLabel("ชื่อ-นามสกุล: " + model.getApName());
    lblApAddress = new JLabel("ที่อยู่: " + model.getApAddress());
    lblApChw = new JLabel("จังหวัด: " + model.getApChw());
    lblApSex = new JLabel("เพศ: " + model.getApSex());
    lblApEd = new JLabel("วุฒิการศึกษาสูงสุด: " + model.getApEd());
    lblApMaj = new JLabel("วิชาเอก: " + model.getApMaj());

    lblQualDesc1 = new JLabel("ความรู้ความสามารถ 1: " + model.getQualDesc1());
    lblQualDesc2 = new JLabel("ความรู้ความสามารถ 2: " + model.getQualDesc2());
    lblQualDesc3 = new JLabel("ความรู้ความสามารถ 3: " + model.getQualDesc3());
    lblQualDesc4 = new JLabel("ความรู้ความสามารถ 4: " + model.getQualDesc4());
    lblQualDesc5 = new JLabel("ความรู้ความสามารถ 5: " + model.getQualDesc5());

    lblPosName1 = new JLabel("ตำแหน่งที่ต้องการสมัคร 1: " + model.getPosName1());
    lblPosName2 = new JLabel("ตำแหน่งที่ต้องการสมัคร 2: " + model.getPosName2());
    lblPosName3 = new JLabel("ตำแหน่งที่ต้องการสมัคร 3: " + model.getPosName3());
  }

  public void defineButton() {
    btnEdit = new JButton("แก้ไข");
    btnEdit.setBorder(null);
  }

  public void setLabelsBounds() {
    lblTitle.setBounds(275, 30, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblApId.setBounds(LBL_START_X, 60, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblApId.setBounds(LBL_START_X, 90, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblApName.setBounds(LBL_START_X, 120, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblApAddress.setBounds(LBL_START_X, 150, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblApChw.setBounds(LBL_START_X, 180, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblApSex.setBounds(LBL_START_X, 210, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblApEd.setBounds(LBL_START_X, 240, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblApMaj.setBounds(LBL_START_X, 270, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);

    lblQualDesc1.setBounds(LBL_START_X, 310, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblQualDesc2.setBounds(LBL_START_X, 340, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblQualDesc3.setBounds(LBL_START_X, 370, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblQualDesc4.setBounds(LBL_START_X, 400, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblQualDesc5.setBounds(LBL_START_X, 430, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);

    lblPosName1.setBounds(LBL_START_X, 470, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblPosName2.setBounds(LBL_START_X, 500, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
    lblPosName3.setBounds(LBL_START_X, 530, LBL_WIDTH_APPDESC, COMPONENT_HEIGHT);
  }

  public void setButtonBounds() {
    btnEdit.setBounds(300, 575, BUTTON_WIDTH, BUTTON_HEIGHT);
  }

  public void addComponentsIntoContainer() {
    container.add(lblTitle);
    container.add(lblApId);
    container.add(lblApName);
    container.add(lblApAddress);
    container.add(lblApChw);
    container.add(lblApSex);
    container.add(lblApEd);
    container.add(lblApMaj);

    container.add(lblQualDesc1);
    container.add(lblQualDesc2);
    container.add(lblQualDesc3);
    container.add(lblQualDesc4);
    container.add(lblQualDesc5);

    container.add(lblPosName1);
    container.add(lblPosName2);
    container.add(lblPosName3);

    container.add(btnEdit);
  }

  public void setLabelsFont() {
    lblTitle.setFont(BOLD_TAHOMA_18);

    lblApId.setFont(PLAIN_TAHOMA_16);
    lblApName.setFont(PLAIN_TAHOMA_16);
    lblApAddress.setFont(PLAIN_TAHOMA_16);
    lblApChw.setFont(PLAIN_TAHOMA_16);
    lblApSex.setFont(PLAIN_TAHOMA_16);
    lblApEd.setFont(PLAIN_TAHOMA_16);
    lblApMaj.setFont(PLAIN_TAHOMA_16);

    lblQualDesc1.setFont(PLAIN_TAHOMA_16);
    lblQualDesc2.setFont(PLAIN_TAHOMA_16);
    lblQualDesc3.setFont(PLAIN_TAHOMA_16);
    lblQualDesc4.setFont(PLAIN_TAHOMA_16);
    lblQualDesc5.setFont(PLAIN_TAHOMA_16);

    lblPosName1.setFont(PLAIN_TAHOMA_16);
    lblPosName2.setFont(PLAIN_TAHOMA_16);
    lblPosName3.setFont(PLAIN_TAHOMA_16);

    btnEdit.setFont(BOLD_TAHOMA_16);
  }

  public JLabel getLblTitle() {
    return lblTitle;
  }

  public JLabel getLblApId() {
    return lblApId;
  }

  public JLabel getLblApName() {
    return lblApName;
  }

  public JLabel getLblApAddress() {
    return lblApAddress;
  }

  public JLabel getLblApChw() {
    return lblApChw;
  }

  public JLabel getLblApSex() {
    return lblApSex;
  }

  public JLabel getLblApEd() {
    return lblApEd;
  }

  public JLabel getLblApMaj() {
    return lblApMaj;
  }

  public JLabel getLblQualDesc1() {
    return lblQualDesc1;
  }

  public JLabel getLblQualDesc2() {
    return lblQualDesc2;
  }

  public JLabel getLblQualDesc3() {
    return lblQualDesc3;
  }

  public JLabel getLblQualDesc4() {
    return lblQualDesc4;
  }

  public JLabel getLblQualDesc5() {
    return lblQualDesc5;
  }

  public JLabel getLblPosName1() {
    return lblPosName1;
  }

  public JLabel getLblPosName2() {
    return lblPosName2;
  }

  public JLabel getLblPosName3() {
    return lblPosName3;
  }
}
